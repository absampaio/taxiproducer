package com.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;

public class TaxiProducer extends Thread {

    File input;
    int speedup;
    int replays;
    int refresh;
    String topic;
    String acks;

    TaxiProducer(File input, int speedup, int replays, int refresh, String topic, String acks) {
        this.input = input;
        this.speedup = speedup;
        this.replays = replays;
        this.refresh = refresh;
        this.topic = topic;
        this.acks = acks;
    }

    public void run() {

        Properties env = System.getProperties();
        Properties props = new Properties();

        props.put("zk.connect", env.getOrDefault("zk.connect", "smd1.cloudapp.net:2181/"));
        props.put("bootstrap.servers", env.getOrDefault("bootstrap.servers", "smd1.cloudapp.net:9092"));
        props.put(ProducerConfig.ACKS_CONFIG, acks);
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        props.put("key.serializer", org.apache.kafka.common.serialization.StringSerializer.class);
        props.put("value.serializer", org.apache.kafka.common.serialization.StringSerializer.class);

        System.err.println(props);

        KafkaProducer<String, String> producer = new KafkaProducer<>(props);

        try {
            long count = 0;
            long start = System.currentTimeMillis(); //--
            for (int i = 0; i < replays; i++){
                InputStream gin = new GZIPInputStream(new FileInputStream(input));
                BufferedReader br = new BufferedReader(new InputStreamReader(gin));
                String line;
                long maxLatency = -1L;
                long totalLatency = 0;
                int reportingInterval = refresh;
                while ((line = br.readLine()) != null) {
                    ProducerRecord<String, String> data = new ProducerRecord<>(topic, line);
                    long sendStart = System.currentTimeMillis();
                    producer.send(data);
                    long sendEllapsed = System.currentTimeMillis() - sendStart;
                    maxLatency = Math.max(maxLatency, sendEllapsed);
                    totalLatency += sendEllapsed;
                    if (count % reportingInterval == 0) {
                        System.out.printf("%d  max latency = %d ms, avg latency = %.5f\n",
                                count,
                                maxLatency,
                                (totalLatency / (double) reportingInterval));
                        totalLatency = 0L;
                        maxLatency = -1L;
                    }
                    count++;
                    Thread.sleep(sendEllapsed/speedup);
                }

                br.close();
            }

            long ellapsed = System.currentTimeMillis() - start;//--
            double msgsSec = 1000.0 * count / (double) ellapsed;//--
            //double mbSec = msgsSec * (recordSize + Records.LOG_OVERHEAD) / (1024.0 * 1024.0);
            System.out.printf("%d records sent in %d ms ms. %.2f records per second", count, ellapsed, msgsSec);//--


        } catch (Exception x) {
            x.printStackTrace();
        }
        producer.close();
    }

    public static void main(String[] args) {

        System.err.println( Arrays.asList( args ));
        if (args.length != 6) {
            System.err.println("usage: java KafkaStreamSource filename.gz speedup #replays stats_refresh_rate topic #acks");
            System.exit(0);
        }
        new TaxiProducer(new File(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]),Integer.parseInt(args[3]),args[4],args[5]).start();
    }
}